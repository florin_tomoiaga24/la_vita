<?php
/**
 * The7 theme.
 *
 * @since   1.0.0
 *
 * @package The7
 */

defined( 'ABSPATH' ) || exit;

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
require trailingslashit( get_template_directory() ) . 'inc/init.php';



function cosmedica_widgets_init() {
    
    
    register_sidebar( array(
		'name'          => __( 'Footer Nav', 'twentysixteen' ),
		'id'            => 'footer_nav',
		'description'   => __( 'footer_nav', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Description', 'twentysixteen' ),
		'id'            => 'footer_description',
		'description'   => __( 'footer_description', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	



	
	

}
add_action( 'widgets_init', 'cosmedica_widgets_init' );