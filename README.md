# Download

Make sure you download https://cloud.google.com/sdk/docs/quickstart-windows (gcloud)

# Set up account

Check the currently connected acount

`$ gcloud auth list`

Check the projects list

`$ gcloud projects list`

Set up your project.

`$ gcloud config set project lavita-de`


# After modifying something

1. Make sure you put it on bitbucket (git push)
2. Make sure you deploy it: 

`$ gcloud app deploy`